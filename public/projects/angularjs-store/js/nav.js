var app = angular.module('nav-directive', []);

app.directive('navigationTabs', function(){
  return {
    restrict: 'E',
    templateUrl: 'nav.html',
    controller: function(){
      this.tab = 1;

      this.isSet = function(checkTab) {
        return this.tab === checkTab;
      };

      this.setTab = function(activeTab) {
        this.tab = activeTab;
      };
    },
    controllerAs: 'navtab'
  }
});