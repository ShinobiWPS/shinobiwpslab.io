/*
	Intoino's function file
*/

var opts = {
lines: 17, // The number of lines to draw
length: 0, // The length of each line
width: 5, // The line thickness
radius: 49, // The radius of the inner circle
corners: 1, // Corner roundness (0..1)
rotate: 24, // The rotation offset
direction: 1, // 1: clockwise, -1: counterclockwise
color: '#006779', // #rgb or #rrggbb or array of colors
speed: 1, // Rounds per second
trail: 64, // Afterglow percentage
shadow: false, // Whether to render a shadow
hwaccel: false, // Whether to use hardware acceleration
className: 'spinner', // The CSS class to assign to the spinner
zIndex: 2e9, // The z-index (defaults to 2000000000)
top: '50%', // Top position relative to parent
left: '50%' // Left position relative to parent
};

/**
 * Push the code for compiling operation.
 */
function pushIt(){
    tabClick("arduino");
    var arduinoName = document.getElementById('arduino_name');
    tabClick("blocks");

    if(arduinoName.value=="" || arduinoName.value.length < 4){
        alert("Inserisci un nome per il tuo iCode, deve essere lungo almeno 4 caratteri!");
    }else{
        
        tabClick("xml");
        var arduinoXML = document.getElementById('content_xml');
        
        tabClick("arduino");
        var arduinoTextarea = document.getElementById('content_arduino');
        
        tabClick("blocks");
        var target = document.getElementById('content_blocks');

        var data = {name: arduinoName.value, value: arduinoTextarea.value, xml: arduinoXML.value, email: email};

        // Spinner
        var spinner = new Spinner(opts).spin(target);
        $(target).data('spinner', spinner);
        
        
        // send values to API
        var url = "https://be.intoino.com/editorApi/pushCode";
        var pushButton = document.getElementById('push_button');
        pushButton.disabled = true;
        $.post(url, data, function(response){
                console.log(response);
                if(response['log'] == "OK"){
                    alert("Compilation Done!");
                }else{
                    $(target).data('spinner').stop();
                    alert(response['log'])
                }
                  $(target).data('spinner').stop();
                pushButton.disabled = false;
                arduinoName.value = "";
            }
        );
    }

}

function share_send(){
//Codice su cui basarsi per l'icona-upload    
//var fd = new FormData();
//fd.append( 'chiave', OGGETTOFILE, 'nomefile.estensione' );
//fd.append( 'chiave', 'valore');
//fd.append( 'chiave', 'valore');
//fd.append( 'chiave', 'valore');
//fd.append( 'chiave', 'valore');
//fd.append( 'chiave', 'valore');
//
//$.ajax({
//url: 'http://example.com/script.php',
//data: fd,
//processData: false,
//contentType: false,
//type: 'POST',
//success: function(data){
//alert(data);
//}
//});
//FINE Codice su cui basarsi per l'icona-upload
    var target = document.getElementById('content_blocks')
    var projectName = document.getElementById('public-name');
    if(projectName.value === "" || projectName.value.length < 4){
        alert("Inserisci un nome per il tuo iCode, deve essere lungo almeno 4 caratteri!");
    }else{
        var projectDescription = document.getElementById('description');             
        var projectDifficulty = document.getElementById('difficulty')
        var projectArduino = document.getElementById('content_arduino');
        var projectXML = document.getElementById('content_xml');
        var data = {
            publicName: projectName.value, 
            abstract: projectDescription.value,
            difficult: projectDifficulty.value,
            iCodeZip: projectArduino.value, 
            XML: projectXML.value, 
            email: email    
        };
        var spinner = new Spinner(opts).spin(target);
        $(target).data('spinner', spinner);
        var url = "https://be.intoino.com/editorApi/shareIcode";
        var shareButton = document.getElementById('sharebutton');
        shareButton.disabled = true;
        $.post(url, data, function(response){
                console.log(response);
                if(response['status'] != "OK")
                    alert(response['log']);
                $(target).data('spinner').stop();
                shareButton.disabled = false;
                projectName.value = "";
            }
        );
    }
}
function stopSpinner(){
    console.log("STOP SPINNER");
    $('#content_blocks').data('spinner').stop();
}

 
 
