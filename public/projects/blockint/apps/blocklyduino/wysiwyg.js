function xml2hw(){ 
	var unique=function(r){var n,e,u,f=[],i=r.length;for(e=0;i>e;e++){for(n=void 0,u=0;u<f.length;u++)if(r[e]===f[u]){n=!0;break}n||f.push(r[e])}return f};

	var xmlTextarea = document.getElementById('content_xml');
	var xmlDom = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
	var xmlText = Blockly.Xml.domToPrettyText(xmlDom);
	xmlTextarea.value = xmlText;
	xmlTextarea.focus();
	var dirty_modules=document.getElementById('content_xml').value.split("<block type=\"grove_");
	var dirty_pins=document.getElementById('content_xml').value.split("<field name=\"PIN\">");
	dirty_modules.splice(0, 1);//remove xml header
	dirty_pins.splice(0, 1);//remove xml header
	var modules = [];
	var pins = [];
	for (var z=0; z < dirty_modules.length; z++) {
		modules[z] = dirty_modules[z].split('"')[0];//variabile senza testo extra,con doppioni
		pins[z] = dirty_pins[z].split("<")[0];//variabile senza testo extra,con doppioni
	}
	for(a=0;a < pins.lenght; a++){
		if (pins[a].indexOf('D') === 0); //pulitura dei pin con la D inziale, arduino non li legge
		dirty_modules.splice(0, 1);
	}
	check_error:
	for(var i=1; i < modules.length; i++){
		for (var j=0; j < i; j++) {
			if ( pins[i] == pins[j] ) {
				if ( modules[i] == modules[j] ) {
					continue;
				}
				else{
					alert("Errore: i componenti " + modules[i] + " e " + modules[j] +" si trovano sullo stesso PIN, ovvero, il PIN numero " + pins[i]);
         			for (var x = 0; x < TABS_.length; x++) {
         			  var name = TABS_[x];
         			  document.getElementById('tab_' + name).className = 'taboff';
         			  document.getElementById('content_' + name).style.visibility = 'hidden';
         			}
         			document.getElementById('tab_blocks').className = 'tabon';
         			document.getElementById('content_blocks').style.visibility = 'visible';
         			renderContent();
         			break check_error;
				}
			}
			else{
				continue;
			}
		}
	}
	console.log('I pin utilizzati sono: ' + pins + '\n' + 'I componenti utilizzati sono: ' + modules);
	console.log("finito");
	var tds = document.getElementsByTagName("td");
	for (var r = 0; r < tds.length; r++) {
	    tds[r].style.background = "none";
	}
	$( ".wire" ).css( "display", "none" );
	for(k=0;k < modules.length;k++){
		slot = pins[k] + "-slot";
		wire =  "wire-" + pins[k];
		console.log(slot);
		console.log(wire);
		svg_module = "svg/grove_" + modules[k] + ".svg";
		console.log(svg_module);
		document.getElementById(slot).style.background = "url(" + svg_module + ") center center";
		document.getElementById(wire).style.display = "inherit"; 
		console.log("finito dentro for");
	}
	console.log("finito fuori for");
}



