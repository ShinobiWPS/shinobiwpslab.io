/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Fred Lin.
 * https://github.com/gasolin/BlocklyDuino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating Arduino blocks.
 * @author gasolin@gmail.com (Fred Lin)
 */
'use strict';

//To support syntax defined in http://arduino.cc/en/Reference/HomePage

goog.provide('Blockly.Blocks.base');

goog.require('Blockly.Blocks');

//--------------SOCIAL NETWORKS-----------

Blockly.Blocks['social_twitter_post'] = {
category: 'Social',
helpUrl: 'www.twitter.com',
  init: function() {
    this.setColour(225);
    this.appendValueInput("twMsg", String)
        .setCheck("String")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Twitter Post")
        .appendField(new Blockly.FieldImage("https://g.twimg.com/Twitter_logo_blue.png", 20, 20, "*"));
    this.setInputsInline(true);
    this.setPreviousStatement(true, "null");
    this.setNextStatement(true, "null");
    this.setTooltip('');
  }
};

Blockly.Blocks['Facebook_post'] = {
category: 'Social',
helpUrl: 'www.facebook.com',
init: function() {
    this.setColour(210);
    this.appendDummyInput("")
    .appendField("Facebook post")
    .appendField(new Blockly.FieldImage("https://www.facebookbrand.com/img/assets/asset.f.logo.lg.png", 16, 16))
    .appendField("Privacy")
    .appendField(new Blockly.FieldDropdown([[Blockly.Msg.SOCIAL_FACEBOOK_PRIVACY_ME, "only_me"], [Blockly.Msg.SOCIAL_FACEBOOK_PRIVACY_FRIENDS, "only_friends"], [Blockly.Msg.SOCIAL_FACEBOOK_PRIVACY_PUBLIC, "public_post"]]), "Privacy");
    this.appendValueInput("MESSAGE", String)
    .setCheck('String')
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(true);
    this.setTooltip(Blockly.Msg.SOCIAL_FACEBOOK_TOOLTIP);
    
}
};

Blockly.Blocks['send_email_recip'] = {
  init: function() {
    this.setHelpUrl('https://en.wikipedia.org/wiki/Email');
    this.setColour(195);
    this.appendDummyInput()
        .appendField("Email")
        .appendField(new Blockly.FieldImage("http://be.intoino.com/img/email_color.png", 20, 20, "*"));
    this.appendValueInput("rec")
        .setCheck("String")
        .appendField(Blockly.Msg.SOCIAL_EMAIL_RECIPIENT);
    this.appendValueInput("msg")
        .setCheck("String")
        .appendField(Blockly.Msg.SOCIAL_EMAIL_MEX);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('');
  }
};

Blockly.Blocks['send_email_myself'] = {
  init: function() {
    this.setHelpUrl('https://en.wikipedia.org/wiki/Email');
    this.setColour(195);
    this.appendDummyInput()
        .appendField(Blockly.Msg.SOCIAL_EMAIL_MYSELF_TITLE)
        .appendField(new Blockly.FieldImage("http://be.intoino.com/img/email_color.png", 20, 20, "*"));
    this.appendValueInput("msg")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(Blockly.Msg.SOCIAL_EMAIL_MEX);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('');
  }
};

Blockly.Blocks['gss_add_entry'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(105);
    this.appendDummyInput()
        .appendField("Google SpreadSheet")
        .appendField(new Blockly.FieldImage("http://be.intoino.com/img/spreadsheet_color.png", 20, 20, "*"));
    this.appendValueInput("fname")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(Blockly.Msg.SOCIAL_GSS_FILE_NAME);
    this.appendValueInput("sensor_name")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(Blockly.Msg.SOCIAL_GSS_SENSOR_NAME);
    this.appendValueInput("value")
        .setCheck("null")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(Blockly.Msg.SOCIAL_GSS_SENSOR_VALUE);
    this.appendValueInput("delay")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField(Blockly.Msg.SOCIAL_GSS_DELAY);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};
//------------------------END SOCIAL--------------------


Blockly.Blocks['base_delay'] = {
    helpUrl: 'http://arduino.cc/en/Reference/delay',
    init: function() {
        this.setColour(120);
        this.appendValueInput("DELAY_TIME", 'Number')
            .appendField(Blockly.Msg.MATH_DELAY_TITLE)
            .setCheck('Number');
        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip(Blockly.Msg.MATH_DELAY_TOOLTIP);
    }
};


Blockly.Blocks['base_map'] = {
    helpUrl: 'http://arduino.cc/en/Reference/map',
    init: function() {
        this.setColour(230);
        this.appendValueInput("NUM", 'Number')
            .appendField(Blockly.Msg.MATH_MAP_TITLE)
            .setCheck('Number');
        this.appendValueInput("DMAX", 'Number')
            .appendField(Blockly.Msg.MATH_MAP_TEXT)
            .setCheck('Number');
        this.appendDummyInput()
              .appendField("]");
        this.setInputsInline(true);
        this.setOutput(true);
        this.setTooltip(Blockly.Msg.MATH_MAP_TOOLTIP);
    }
};

Blockly.Blocks['inout_buildin_led'] = {
   helpUrl: 'http://arduino.cc/en/Reference/DigitalWrite',
   init: function() {
     this.setColour(190);
     this.appendDummyInput()
	       .appendField(Blockly.Msg.ADVANCE_BUILDIN_LED_TITLE)
           .appendField(Blockly.Msg.GROVE_DEFAULT_STAT)
	       .appendField(new Blockly.FieldDropdown([[Blockly.Msg.GROVE_DEFAULT_STAT_HIGH, "HIGH"], [Blockly.Msg.GROVE_DEFAULT_STAT_LOW, "LOW"]]), "STAT");
     this.setPreviousStatement(true, null);
     this.setNextStatement(true, null);
     this.setTooltip(Blockly.Msg.ADVANCE_BUILDIN_LED_TOOLTIP);
   }
};

Blockly.Blocks['inout_digital_write'] = {
  helpUrl: 'http://arduino.cc/en/Reference/DigitalWrite',
  init: function() {
    this.setColour(230);
    this.appendDummyInput()
	      .appendField(Blockly.Msg.ADVANCE_DIGITAL_WRITE_TITLE)
	      .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
      	.appendField(Blockly.Msg.GROVE_DEFAULT_STAT)
      	.appendField(new Blockly.FieldDropdown([[Blockly.Msg.GROVE_DEFAULT_STAT_HIGH, "HIGH"], [Blockly.Msg.GROVE_DEFAULT_STAT_LOW, "LOW"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(Blockly.Msg.ADVANCE_DIGITAL_WRITE_TOOLTIP);
  }
};

Blockly.Blocks['inout_digital_read'] = {
  helpUrl: 'http://arduino.cc/en/Reference/DigitalRead',
  init: function() {
    this.setColour(230);
    this.appendDummyInput()
	      .appendField(Blockly.Msg.ADVANCE_DIGITAL_READ_TITLE)
	      .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip(Blockly.Msg.ADVANCE_DIGITAL_READ_TOOLTIP);
  }
};

Blockly.Blocks['inout_analog_read'] = {
  helpUrl: 'http://arduino.cc/en/Reference/AnalogRead',
  init: function() {
    this.setColour(230);
    this.appendDummyInput()
        .appendField(Blockly.Msg.ADVANCE_ANALOG_READ_TITLE)
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN");
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.ADVANCE_ANALOG_READ_TOOLTIP);
  }
};

Blockly.Blocks['inout_highlow'] = {
  helpUrl: 'http://arduino.cc/en/Reference/Constants',
  init: function() {
    this.setColour(230);
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([[Blockly.Msg.GROVE_DEFAULT_STAT_HIGH, "HIGH"], [Blockly.Msg.GROVE_DEFAULT_STAT_LOW, "LOW"]]), 'BOOL')
    this.setOutput(true, 'Boolean');
    this.setTooltip('');
  }
};

Blockly.Blocks['serial_print'] = {
  helpUrl: 'http://www.arduino.cc/en/Serial/Print',
  init: function() {
    this.setColour(230);
    this.appendValueInput("CONTENT", 'String')
        .appendField(Blockly.Msg.ADVANCE_SERIAL_PRINT_TITLE);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(Blockly.Msg.ADVANCE_SERIAL_PRINT_TOOLTIP);
  }
};
