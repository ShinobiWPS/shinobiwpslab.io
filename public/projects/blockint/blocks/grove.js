//http://www.seeedstudio.com/wiki/GROVE_System
//http://www.seeedstudio.com/depot/index.php?main_page=advanced_search_result&search_in_description=1&keyword=grovefamily
//support starter bundle example http://www.seeedstudio.com/wiki/GROVE_-_Starter_Kit_V1.1b

/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Fred Lin.
 * https://github.com/gasolin/BlocklyDuino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating seeeduino grove blocks.
 * @author gasolin@gmail.com (Fred Lin)
 */

goog.provide('Blockly.Blocks.grove');

goog.require('Blockly.Blocks');

//----------------NEW---------------------------

Blockly.Blocks['grove_moisture'] = {
     helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#LED',
     init: function() {
     this.setColour(120);
     this.appendDummyInput()
         .appendField(Blockly.Msg.GROVE_MOISTURE_TITLE)
         .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/b/bf/Twig_-_Moisture_Sensor.jpg/200px-Twig_-_Moisture_Sensor.jpg", 64, 64))
         .appendField("PIN#")
         .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
     this.setOutput(true, 'Number');
     this.setTooltip(Blockly.Msg.GROVE_MOISTURE_TOOLTIP);
   }
 };
 
Blockly.Blocks['grove_slide_pot'] = {
helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Potentiometer',
init: function() {
    this.setColour(10);
    this.appendDummyInput()
    .appendField(Blockly.Msg.GROVE_SLIDE_POT_TITLE)
    .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/6/60/Sliding1.JPG/200px-Sliding1.JPG", 64, 64))
    .appendField("PIN#")
    .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN");
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.GROVE_SLIDE_POT_TOOLTIP);
}
};

// Blockly.Blocks['social_twitter_post'] = {
//   init: function() {
//     this.setHelpUrl('http://www.example.com/');
//     this.setColour(225);
//     this.appendValueInput("twMsg")
//         .setCheck("String")
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField("Twitter Post")
//         .appendField(new Blockly.FieldImage("https://g.twimg.com/Twitter_logo_blue.png", 20, 20, "*"));
//     this.setInputsInline(true);
//     this.setPreviousStatement(true, "null");
//     this.setNextStatement(true, "null");
//     this.setTooltip('');
//   }
// };

Blockly.Blocks['grove_gas'] = {
category: 'Grove Analog',
helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Gas_Sensor',
init: function() {
    this.setColour(110);
    this.appendDummyInput("")
    .appendField(Blockly.Msg.GROVE_SENSOR_GAS_TITLE)
    .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/depot/images/product/Gas%20Sensor%20MQ.jpg", 64, 64))
    .appendField("PIN#")
    .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.GROVE_SENSOR_GAS_TOOLTIP);
}
};

Blockly.Blocks['grove_thumb_joystick'] = {
  category: 'Grove Analog',
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Thumb_Joystick',
  init: function() {
    this.setColour(10);
    this.appendDummyInput("")
    .appendField("Thumb Joystick")
    .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/e/e0/Twig_-_Thumb_Joystick_v0.9b.jpg/200px-Twig_-_Thumb_Joystick_v0.9b.jpg", 64, 64))
    .appendField("PIN#")
    .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
    .appendField("axis")
    .appendField(new Blockly.FieldDropdown([["x", "x"],  ["y", "y"]]), "AXIS");
    this.setOutput(true, 'Number');
    this.setTooltip('output two analog values(200~800) representing two directions');
  }
};

Blockly.Blocks['grove_light_sensor'] = {
category: 'Grove Analog',
helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Light_Sensor',
init: function() {
    this.setColour(10);
    this.appendDummyInput("")
    .appendField(Blockly.Msg.GROVE_SENSOR_LIGHT_TITLE)
    .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/1/1c/Twig-Light.jpg/200px-Twig-Light.jpg", 64, 64))
    .appendField("PIN#")
    .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.GROVE_SENSOR_LIGHT_TOOLTIP);
}
};

Blockly.Blocks['grove_led_PWM'] = {
category: 'Grove',
helpUrl: 'http://www.arduino.cc/en/Tutorial/PWM',
init: function() {
    this.setColour(190);
    this.appendDummyInput("")
    .appendField("LED PWM")
    .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/e/e0/LED1.jpg/400px-LED1.jpg", 64, 64))
    .appendField("PIN#")
    .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldPWM), "PIN")
    this.appendValueInput("LEDvalue", 'Number')
    .setCheck('Number')
    .setAlign(Blockly.ALIGN_RIGHT)
    .appendField(Blockly.Msg.GROVE_LED_PWM);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setInputsInline(true);
    this.setTooltip(Blockly.Msg.GROVE_LED_PWM_TOOLTIP);
}
};

Blockly.Blocks['grove_hall_sensor'] = {
category: 'Grove',
helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Hall_Sensor',
init: function() {
    this.setColour(190);
    this.appendDummyInput("")
    .appendField(Blockly.Msg.GROVE_SENSOR_HALL_TITLE)
    .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/0/04/Grove_-_Hall_Sensor.jpg/200px-Grove_-_Hall_Sensor.jpg", 64, 64))
    .appendField("PIN#")
    .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip(Blockly.Msg.GROVE_SENSOR_HALL_TOOLTIP);
}
};

Blockly.Blocks['grove_LEDbar'] = {
category: 'Grove',
helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_LED_Bar',
init: function() {
    this.setColour(190);
    this.appendDummyInput("")
    .appendField(Blockly.Msg.GROVE_LED_BAR_TITLE)
    .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/b/b8/Bar.jpg", 64, 64))
    .appendField("PIN#")
    .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
    this.appendValueInput("LEDbar", 'Number')
    .setCheck('Number')
    .setAlign(Blockly.ALIGN_RIGHT)
    .appendField(Blockly.Msg.GROVE_LED_BAR_TEXT);
    //this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(Blockly.Msg.GROVE_LED_BAR_TOOLTIP);
}
};

Blockly.Blocks['grove_led'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#LED',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("LED")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/e/e0/LED1.jpg/400px-LED1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
        .appendField(Blockly.Msg.GROVE_DEFAULT_STAT)
        .appendField(new Blockly.FieldDropdown([[Blockly.Msg.GROVE_DEFAULT_STAT_HIGH, "HIGH"], [Blockly.Msg.GROVE_DEFAULT_STAT_LOW, "LOW"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(Blockly.Msg.GROVE_LED_TOOLTIP);
  }
};

Blockly.Blocks['grove_button'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Button',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_BUTTON_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/9/93/Button1.jpg/400px-Button1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip(Blockly.Msg.GROVE_BUTTON_TOOLTIP);
  }
};

Blockly.Blocks['grove_touch'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Touch_Sensor',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_TOUCH_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/0/01/Grove_-_touch_sensor_Photo.jpg/400px-Grove_-_touch_sensor_Photo.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip(Blockly.Msg.GROVE_TOUCH_TOOLTIP);
  }
};


Blockly.Blocks['grove_I2C_lcd_print'] = {
     category: 'Grove LCD',
     helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_LCD_RGB_Backlight',
     init: function() {
     this.setColour(190);
     this.appendDummyInput("")
     .appendField("LCD on I2C")
     .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/0/03/Serial_LEC_RGB_Backlight_Lcd.jpg/400px-Serial_LEC_RGB_Backlight_Lcd.jpg", 64, 64))
     .appendField("PIN#")
         .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldI2C), "PIN");
     this.appendValueInput("TEXT", 'String')
     .setCheck('String')
     .setAlign(Blockly.ALIGN_RIGHT)
     .appendField(Blockly.Msg.GROVE_LCD_I2C_LINE1);
     this.appendValueInput("TEXT2", 'String')
     .setCheck('String')
     .setAlign(Blockly.ALIGN_RIGHT)
     .appendField(Blockly.Msg.GROVE_LCD_I2C_LINE2)
     this.appendValueInput("DELAY_TIME", 'Number')
     .setCheck('Number')
     .setAlign(Blockly.ALIGN_RIGHT)
     .appendField(Blockly.Msg.GROVE_LCD_I2C_DELAY);
         
         
     this.appendValueInput("COLOUR", 'Colour')
         .setCheck('Colour')
         .setAlign(Blockly.ALIGN_RIGHT)
         .appendField(Blockly.Msg.GROVE_LCD_I2C_COLOR);
         
    this.appendDummyInput()
         .appendField(new Blockly.FieldCheckbox("FALSE"), "NAME")
         .setAlign(Blockly.ALIGN_RIGHT)
         .appendField(Blockly.Msg.GROVE_LCD_I2C_SCROLL);
    //    .appendField(new Blockly.FieldColour("#00ff00"), "RGB2");
   //       .appendField("Color 1")
   //       .appendField(new Blockly.FieldColour("#00ff00"), "RGB0");
   //        var colour_rgb = this.getTitleValue('RGB0');
   //       container.setAttribute('RGB' + x, colour_rgb);
   //       var colour_rgb = this.getTitleValue('RGB'+n);
   //       code += "DataDealWithAndSend_"+dropdown_pin+"("+hexToR(colour_rgb)+", "+hexToG(colour_rgb)+", "+hexToB(colour_rgb)+"); // first node data\n";

     this.setPreviousStatement(true);
     this.setNextStatement(true);
     this.setTooltip(Blockly.Msg.GROVE_LCD_I2C_TOOLTIP);
 }
 };



//---------------------------------------------

Blockly.Blocks['grove_rotary_angle'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Potentiometer',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_ROTARY_ANGLE_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/5/59/Potentiometer1.jpg/400px-Potentiometer1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN");
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.GROVE_ROTARY_ANGLE_TOOLTIP);
  }
};

Blockly.Blocks['grove_tilt_switch'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Tilt_switch',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Tilt Switch")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/9/95/Tilt1.jpg/400px-Tilt1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip('When the switch is level it is open, and when tilted, the switch closes.');
  }
};

Blockly.Blocks['grove_piezo_buzzer'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/GROVE_-_Starter_Kit_V1.1b#Grove_.E2.80.93_Buzzer',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_BUZZER_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/e/ed/Buzzer1.jpg/400px-Buzzer1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
        .appendField(Blockly.Msg.GROVE_DEFAULT_STAT)
        .appendField(new Blockly.FieldDropdown([[Blockly.Msg.GROVE_DEFAULT_STAT_HIGH, "HIGH"], [Blockly.Msg.GROVE_DEFAULT_STAT_LOW, "LOW"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(Blockly.Msg.GROVE_BUZZER_TOOLTIP);
  }
};

Blockly.Blocks['grove_relay'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Relay',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_RELAY_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/0/04/Twig-Relay1.jpg/400px-Twig-Relay1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
        .appendField(Blockly.Msg.GROVE_DEFAULT_STAT)
        .appendField(new Blockly.FieldDropdown([[Blockly.Msg.GROVE_DEFAULT_STAT_HIGH, "HIGH"], [Blockly.Msg.GROVE_DEFAULT_STAT_LOW, "LOW"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip(Blockly.Msg.GROVE_RELAY_TOOLTIP);
  }
};

Blockly.Blocks['grove_infrared_receiver'] = {
    helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Infrared_Receiver',
    init: function () {
        this.setColour(10);
        this.appendDummyInput()
            .appendField(Blockly.Msg.GROVE_INFRARED_TITLE)
            .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/e/ee/Grove_-_Infrared_Receiver.jpg/400px-Grove_-_Infrared_Receiver.jpg", 64, 64))
            .appendField("PIN#")
            .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
        this.setOutput(true, 'String');
        this.setTooltip(Blockly.Msg.GROVE_INFRARED_TOOLTIP);
    }
};

Blockly.Blocks['grove_temperature_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Project_Seven_-_Temperature',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_SENSOR_TEMP_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/b/b0/Temperature1.jpg/400px-Temperature1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.GROVE_SENSOR_TEMP_TOOLTIP);
  }
};

Blockly.Blocks['grove_temperaturehumidity'] ={
    category: 'Grove Analog',
    helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Temperature_and_Humidity_Sensor',
    init: function() {
        this.setColour(270);
        this.appendDummyInput("")
        .appendField(Blockly.Msg.GROVE_TEMP_HUMIDITY_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/2/2d/Temp_and_Humi_Sensor.jpg/200px-Temp_and_Humi_Sensor.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
        .appendField("T°/ RH")
        .appendField(new Blockly.FieldDropdown([[Blockly.Msg.GROVE_TEMP_HUMIDITY_TEXT1, "temp"], [Blockly.Msg.GROVE_TEMP_HUMIDITY_TEXT2, "hum"]]), "Field")
        this.setOutput(true, "Array");
        this.setTooltip(Blockly.Msg.GROVE_TEMP_HUMIDITY_TOOLTIP);
        }
};

//-----------------------------------------------------------
Blockly.Blocks['grove_serial_lcd_print'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#Serial_LCD',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Serial LCD")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/6/6a/LCD1.jpg/400px-LCD1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.appendValueInput("TEXT", 'String')
        .setCheck('String')
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("print line1");
    this.appendValueInput("TEXT2", 'String')
        .setCheck('String')
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("print line2")
    this.appendValueInput("DELAY_TIME", 'Number')
        .setCheck('Number')
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Delay");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('print text on an 16 character by 2 line LCD.');
  }
};

//grove lcd power on/off
Blockly.Blocks['grove_serial_lcd_power'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#LED',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Serial LCD")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/6/6a/LCD1.jpg/400px-LCD1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Power")
        .appendField(new Blockly.FieldDropdown([["ON", "ON"], ["OFF", "OFF"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Turn LCD power on/off');
  }
};

//scroll left/right/no scroll/blink/noblink
Blockly.Blocks['grove_serial_lcd_effect'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=GROVE_-_Starter_Bundle_V1.0b#LED',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Serial LCD")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/6/6a/LCD1.jpg/400px-LCD1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Effect")
        .appendField(new Blockly.FieldDropdown([["Scroll Left", "LEFT"], ["Scroll Right", "RIGHT"], ["Scroll Auto", "AUTO"]]), "STAT");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Turn LCD power on/off');
  }
};

Blockly.Blocks['grove_sound_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Sound_Sensor',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_SENSOR_SOUND_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/e/e3/Twig-Sound-sensor.jpg/400px-Twig-Sound-sensor.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.GROVE_SENSOR_SOUND_TOOLTIP);
  }
};

Blockly.Blocks['grove_pir_motion_sensor'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_PIR_Motion_Sensor',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField(Blockly.Msg.GROVE_SENSOR_PIR_TITLE)
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/f/fd/Twig-PIR_Motion_Sensor.jpg/400px-Twig-PIR_Motion_Sensor.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
    this.setOutput(true, 'Number');
    this.setTooltip(Blockly.Msg.GROVE_SENSOR_PIR_TOOLTIP);
  }
};

Blockly.Blocks['grove_line_finder'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Line_Finder',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Line Finder")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/8/82/Grovelinefinder1.jpg/400px-Grovelinefinder1.jpg", 64, 64))
	      .appendField("PIN#")
	      .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
    this.setOutput(true, 'Boolean');
    this.setTooltip('Output digital signal so the robot can reliably follow a black line on a white background');
  }
};

Blockly.Blocks['grove_ultrasonic_ranger'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Ultrasonic_Ranger',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
	      .appendField("Ultrasonic Ranger")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/b/b0/Twig_-_Ultrasonic_Ranger2.jpg/200px-Twig_-_Ultrasonic_Ranger2.jpg", 64, 64))
	      .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
        .appendField("unit")
        .appendField(new Blockly.FieldDropdown([["cm", "cm"],  ["inch", "inch"]]), "UNIT");
    this.setOutput(true, 'Boolean');
    this.setTooltip('Non-contact distance measurement module');
  }
};

Blockly.Blocks['grove_motor_shield'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Motor_Shield',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Motor")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/4/4d/Smotoshield2.jpg/400px-Smotoshield2.jpg", 64, 64))
        .appendField(new Blockly.FieldDropdown([["Stop", "stop"], ["Forward", "forward"], ["Right", "right"], ["Left", "left"], ["Backward", "backward"]]), "DIRECTION");
    /*this.appendValueInput("SPEED", 'Number')
        .setCheck('Number')
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Speed");*/
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Drive two brushed DC motors');
  }
};

Blockly.Blocks['grove_thumb_joystick'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Thumb_Joystick',
  init: function() {
    this.setColour(10);
    this.appendDummyInput()
	.appendField("Thumb Joystick")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/images/thumb/e/e0/Twig_-_Thumb_Joystick_v0.9b.jpg/200px-Twig_-_Thumb_Joystick_v0.9b.jpg", 64, 64))
	.appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldAnalog), "PIN")
        .appendField("axis")
        .appendField(new Blockly.FieldDropdown([["x", "x"],  ["y", "y"]]), "AXIS");
    this.setOutput(true, 'Number');
this.setTooltip('output two analog values(200~800) representing two directions');
  }
};

Blockly.Blocks['grove_rgb_led'] = {
  helpUrl: 'http://www.seeedstudio.com/wiki/index.php?title=Twig_-_Chainable_RGB_LED',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
    .appendField("Chainable RGB LED")
        .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/depot/images/product/chanbalelednb1.jpg", 64, 64))
        .appendField("PIN#")
        .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
    this.appendDummyInput("COLOR0")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Color 1")
        .appendField(new Blockly.FieldColour("#00ff00"), "RGB0");
    this.setMutator(new Blockly.Mutator(['grove_rgb_led_item']));
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('256 color LED, currently Chainable feature is not support');
    this.itemCount_ = 1;
  },
  mutationToDom: function() {
    var container = document.createElement('mutation');
    container.setAttribute('items', this.itemCount_);
    for (var x = 0; x < this.itemCount_; x++) {
      var colour_rgb = this.getFieldValue('RGB0');
      //alert(colour_rgb);
      container.setAttribute('RGB' + x, colour_rgb);
    }
    return container;
  },
  domToMutation: function(xmlElement) {
    for (var x = 0; x < this.itemCount_; x++) {
      this.removeInput('COLOR' + x);
    }
    this.itemCount_ = window.parseInt(xmlElement.getAttribute('items'), 10);
    for (var x = 0; x < this.itemCount_; x++) {
      var color = window.parseInt(xmlElement.getAttribute('RGB'+x), "#00ff00");
      var input = this.appendDummyInput('COLOR' + x);
      //if (x == 0) {
        input.setAlign(Blockly.ALIGN_RIGHT)
             .appendField("Color "+(x+1))
             .appendField(new Blockly.FieldColour(color), "RGB" + x);
      //}
    }
    if (this.itemCount_ == 0) {
      this.appendDummyInput('COLOR0')
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("Color 1")
          .appendField(new Blockly.FieldColour("#00ff00"), "RGB0");
    }
  },
  decompose: function(workspace) {
    var containerBlock = new Blockly.Block(workspace,
                                           'grove_rgb_led_container');
    containerBlock.initSvg();
    var connection = containerBlock.getInput('STACK').connection;
    for (var x = 0; x < this.itemCount_; x++) {
      var itemBlock = new Blockly.Block(workspace, 'grove_rgb_led_item');
      itemBlock.initSvg();
      connection.connect(itemBlock.previousConnection);
      connection = itemBlock.nextConnection;
    }
    return containerBlock;
  },
  compose: function(containerBlock) {
    // Disconnect all input blocks and remove all inputs.
    if (this.itemCount_ == 0) {
      this.removeInput('COLOR0');
    } else {
      for (var x = this.itemCount_ - 1; x >= 0; x--) {
        //console.log("cnt:"+x);
        this.removeInput('COLOR' + x);
      }
    }
    /*var top;
    if(this.itemCount_ > 0){
      top = this.itemCount_-1;
    } else {
      top = 0;
    }
    console.log("top:"+top);*/
    this.itemCount_ = 0;
    // Rebuild the block's inputs.
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    while (itemBlock) {
      var colour_rgb = this.getFieldValue('RGB' + this.itemCount_);
      if(colour_rgb==null){
          colour_rgb = "00ff00";
      }
      //console.log("blk:"+this.itemCount_);
      /*if(top>this.itemCount_){
        this.removeInput('COLOR' + this.itemCount_);
      }*/
      var input = this.appendDummyInput('COLOR' + this.itemCount_);
      //if (this.itemCount_ == 0) {
        input.setAlign(Blockly.ALIGN_RIGHT)
             .appendField("Color " + (this.itemCount_+1))
             .appendField(new Blockly.FieldColour(colour_rgb), "RGB" + this.itemCount_);
      //}
      // Reconnect any child blocks.
      if (itemBlock.valueConnection_) {
        input.connection.connect(itemBlock.valueConnection_);
      }
      this.itemCount_++;
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
    if (this.itemCount_ == 0) {
      this.appendDummyInput('COLOR0')
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("Color 1")
          .appendField(new Blockly.FieldColour("#00ff00"), "RGB0");
    }
  }
  /*saveConnections: function(containerBlock) {
    // Store a pointer to any connected child blocks.
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    var x = 0;
    while (itemBlock) {
      var input = this.getInput('COLOR' + x);
      itemBlock.valueConnection_ = input && input.connection.targetConnection;
      x++;
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
  }*/
};

Blockly.Blocks['grove_rgb_led_container'] = {
  // Container.
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Container");
    this.appendStatementInput('STACK');
    this.setTooltip("Add, remove items to reconfigure this chain");
    this.contextMenu = false;
  }
};

Blockly.Blocks['grove_rgb_led_item'] = {
  // Add items.
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
        .appendField("Item");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Add an item to the chain");
    this.contextMenu = false;
  }
};

Blockly.Blocks['grove_bluetooth_slave'] = {
  category: 'Network',
  helpUrl: 'http://www.seeedstudio.com/wiki/Grove_-_Serial_Bluetooth',
  init: function() {
    this.setColour(190);
    this.appendDummyInput()
      .appendField("Bluetooth Slave")
      .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/wiki/File:Twigbt00.jpg", 64, 64))
      .appendField("PIN#")
      .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
    this.appendDummyInput()
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("Name")
      .appendField(new Blockly.FieldTextInput('blocklyduino'), 'NAME');
    this.appendDummyInput()
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("Pincode")
      .appendField(new Blockly.FieldTextInput('0000'), 'PINCODE');
    this.appendStatementInput("RCV")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("Receive");
    this.appendStatementInput("SNT")
      .setAlign(Blockly.ALIGN_RIGHT)
      .appendField("Send");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip('Bluetooth V2.0+EDR slave. Support single slave per board');
  }
};
//http://www.seeedstudio.com/wiki/File:Twig-Temp%26Humi.jpg
//http://www.seeedstudio.com/wiki/Grove-_Temperature_and_Humidity_Sensor

//http://www.seeedstudio.com/wiki/Grove_-_125KHz_RFID_Reader

/*
void setup()
{
	pinMode( 3 , OUTPUT);
	pinMode( 1 , INPUT);
}

void loop()
{
	if (digitalRead( 1))
	{
		digitalWrite( 3 , HIGH);
	}
	else
	{
		digitalWrite( 1 , LOW);
	}
}
*/

//servo block
//http://www.seeedstudio.com/depot/emax-9g-es08a-high-sensitive-mini-servo-p-760.html?cPath=170_171
Blockly.Blocks['grove_servo_move'] = {
    helpUrl: 'http://www.arduino.cc/playground/ComponentLib/servo',
    init: function () {
        this.setColour(190);
        this.appendDummyInput()
            .appendField("Servo")
            .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/depot/images/product/a991.jpg", 64, 64))
            .appendField("PIN#")
            .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN")
        this.appendValueInput("DEGREE", 'Number')
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.GROVE_SERVO_MOVE_TEXT);
        this.appendValueInput("DELAY_TIME", 'Number')
            .setCheck('Number')
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.GROVE_SERVO_MOVE_DELAY);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip(Blockly.Msg.GROVE_SERVO_MOVE_TOOLTIP);
    }
};

Blockly.Blocks['grove_servo_read_degrees'] = {
    helpUrl: 'http://www.arduino.cc/playground/ComponentLib/servo',
    init: function () {
        this.setColour(190);
        this.appendDummyInput()
            .appendField("Servo")
            .appendField(new Blockly.FieldImage("http://www.seeedstudio.com/depot/images/product/a991.jpg", 64, 64))
            .appendField("PIN#")
            .appendField(new Blockly.FieldDropdown(profile.default.BaseShieldDigital), "PIN");
        this.appendDummyInput()
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.GROVE_SERVO_READ_TEXT)
        this.setOutput(true, 'Number');
        this.setTooltip(Blockly.Msg.GROVE_SERVO_READ_TOOLTIP);
    }
};


