/**
 * Visual Blocks Language
 *
 * Copyright 2012 Fred Lin.
 * https://github.com/gasolin/BlocklyDuino
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating Arduino blocks.
 * @author gasolin@gmail.com (Fred Lin)
 */
'use strict';

goog.provide('Blockly.Arduino.base');

goog.require('Blockly.Arduino');

//---------COLOUR--------------------

Blockly.Arduino.colour_blend = function() {
  // Blend two colours together.
  var c1 = Blockly.Arduino.valueToCode(this, 'COLOUR1',
      Blockly.Arduino.ORDER_NONE) || '\'#000000\'';
  var c2 = Blockly.Arduino.valueToCode(this, 'COLOUR2',
      Blockly.Arduino.ORDER_NONE) || '\'#000000\'';
  var ratio = Blockly.Arduino.valueToCode(this, 'RATIO',
      Blockly.Arduino.ORDER_NONE) || 0.5;

  if (!Blockly.Arduino.definitions_['colour_blend']) {
    Blockly.Arduino.definitions_['import_dart_math'] =
        'import \'dart:math\' as Math;';
    var functionName = Blockly.Arduino.variableDB_.getDistinctName(
        'colour_blend', Blockly.Generator.NAME_TYPE);
    Blockly.Arduino.colour_blend.functionName = functionName;
    var func = [];
    func.push('String ' + functionName + '(c1, c2, ratio) {');
    func.push('  ratio = Math.max(Math.min(ratio, 1), 0);');
    func.push('  int r1 = int.parse(\'0x${c1.substring(1, 3)}\');');
    func.push('  int g1 = int.parse(\'0x${c1.substring(3, 5)}\');');
    func.push('  int b1 = int.parse(\'0x${c1.substring(5, 7)}\');');
    func.push('  int r2 = int.parse(\'0x${c2.substring(1, 3)}\');');
    func.push('  int g2 = int.parse(\'0x${c2.substring(3, 5)}\');');
    func.push('  int b2 = int.parse(\'0x${c2.substring(5, 7)}\');');
    func.push('  String rs = (r1 * (1 - ratio) + r2 * ratio).round()' +
              '.toRadixString(16);');
    func.push('  String gs = (g1 * (1 - ratio) + g2 * ratio).round()' +
              '.toRadixString(16);');
    func.push('  String bs = (b1 * (1 - ratio) + b2 * ratio).round()' +
              '.toRadixString(16);');
    func.push('  rs = \'0$rs\';');
    func.push('  rs = rs.substring(rs.length - 2);');
    func.push('  gs = \'0$gs\';');
    func.push('  gs = gs.substring(gs.length - 2);');
    func.push('  bs = \'0$bs\';');
    func.push('  bs = bs.substring(bs.length - 2);');
    func.push('  return \'#$rs$gs$bs\';');
    func.push('}');
    Blockly.Arduino.definitions_['colour_blend'] = func.join('\n');
  }
  var code = Blockly.Arduino.colour_blend.functionName +
      '(' + c1 + ', ' + c2 + ', ' + ratio + ')';
  return [code, Blockly.Arduino.ORDER_UNARY_POSTFIX];
};

Blockly.Arduino.colour_rgb = function() {
  // Compose a colour from RGB components.
  var red = Blockly.Arduino.valueToCode(this, 'RED',
      Blockly.Arduino.ORDER_NONE) || 0;
  var green = Blockly.Arduino.valueToCode(this, 'GREEN',
      Blockly.Arduino.ORDER_NONE) || 0;
  var blue = Blockly.Arduino.valueToCode(this, 'BLUE',
      Blockly.Arduino.ORDER_NONE) || 0;

  if (!Blockly.Arduino.definitions_['colour_rgb']) {
    Blockly.Arduino.definitions_['import_dart_math'] =
        'import \'dart:math\' as Math;';
    var functionName = Blockly.Arduino.variableDB_.getDistinctName(
        'colour_rgb', Blockly.Generator.NAME_TYPE);
    Blockly.Arduino.colour_rgb.functionName = functionName;
    var func = [];
    func.push('String ' + functionName + '(r, g, b) {');
    func.push('  String rs = (Math.max(Math.min(r, 1), 0) * 255).round()' +
              '.toRadixString(16);');
    func.push('  String gs = (Math.max(Math.min(g, 1), 0) * 255).round()' +
              '.toRadixString(16);');
    func.push('  String bs = (Math.max(Math.min(b, 1), 0) * 255).round()' +
              '.toRadixString(16);');
    func.push('  rs = \'0$rs\';');
    func.push('  rs = rs.substring(rs.length - 2);');
    func.push('  gs = \'0$gs\';');
    func.push('  gs = gs.substring(gs.length - 2);');
    func.push('  bs = \'0$bs\';');
    func.push('  bs = bs.substring(bs.length - 2);');
    func.push('  return \'#$rs$gs$bs\';');
    func.push('}');
    Blockly.Arduino.definitions_['colour_rgb'] = func.join('\n');
  }
  var code = Blockly.Arduino.colour_rgb.functionName +
      '(' + red + ', ' + green + ', ' + blue + ')';
  return [code, Blockly.Arduino.ORDER_UNARY_POSTFIX];
};

Blockly.Arduino.colour_picker = function() {
  // Colour picker.
  var code = this.getFieldValue('COLOUR');

  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.colour_random = function () {
    // Colour random.

    var num = Math.floor(Math.random() * Math.pow(2, 24));
    var code = '#' + ('00000' + num.toString(16)).substr(-6);

    return [code, Blockly.Arduino.ORDER_ATOMIC];
};



//--------END COLOUR-------------------


//-------------SOCIAL NETWORKS---------
Blockly.Arduino.Facebook_post = function() {
  Blockly.Arduino.definitions_['define_social'] = '#include <Utilis.h>\n#include <Intoino_Social.h>\nSocial social;';
    var msg = Blockly.Arduino.valueToCode(this, 'MESSAGE',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
    var Privacy = this.getTitleValue('Privacy');
    var code = 'social.FBpost('+msg +','+Privacy+');\n';
  
  return code;
};

Blockly.Arduino.social_twitter_post = function() {
  Blockly.Arduino.definitions_['define_social'] = '#include <Utilis.h>\n#include <Intoino_Social.h>\nSocial social;';
  var msg = Blockly.Arduino.valueToCode(this, 'twMsg',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var code = 'social.TWpost(' + msg + ');\n';
  
  return code;
};

Blockly.Arduino.send_email_recip = function() {
  Blockly.Arduino.definitions_['define_social'] = '#include <Utilis.h>\n#include <Intoino_Social.h>\nSocial social;';
  var rec = Blockly.Arduino.valueToCode(this, 'rec',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var msg = Blockly.Arduino.valueToCode(this, 'msg',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var code = 'social.SendEmail(' + rec + ', ' + msg +');\n';
  
  return code;
};

Blockly.Arduino.send_email_myself = function() {
  Blockly.Arduino.definitions_['define_social'] = '#include <Utilis.h>\n#include <Intoino_Social.h>\nSocial social;';
  var msg = Blockly.Arduino.valueToCode(this, 'msg',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  var code = 'social.SendEmail(' + msg +');\n';
  
  return code;
};


Blockly.Arduino.gss_add_entry = function() {
  Blockly.Arduino.definitions_['define_social'] = '#include <Utilis.h>\n#include <Intoino_Social.h>\nSocial social;';
  var fname = Blockly.Arduino.valueToCode(this, 'fname',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
    var sens = Blockly.Arduino.valueToCode(this, 'sensor_name',
        Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
    var value = Blockly.Arduino.valueToCode(this, 'value',
        Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
    var delay = Blockly.Arduino.valueToCode(this, 'delay',
        Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
     Blockly.Arduino.setups_['delay'] = 'int tdelay = '+delay+';'
  var code = 'social.GSSappend('+ fname + ', ' + sens + ', ' + value +');\n if(tdelay<11) tdelay=11;\n delay(tdelay*1000);\n';
  
  return code;
};


//---------------END SOCIALS

Blockly.Arduino.base_delay = function() {
  var delay_time = Blockly.Arduino.valueToCode(this, 'DELAY_TIME', Blockly.Arduino.ORDER_ATOMIC) || '1000'
  var code = 'delay(' + delay_time + ');\n';
  return code;
};

Blockly.Arduino.base_map = function() {
  var value_num = Blockly.Arduino.valueToCode(this, 'NUM', Blockly.Arduino.ORDER_NONE);
  var value_dmax = Blockly.Arduino.valueToCode(this, 'DMAX', Blockly.Arduino.ORDER_ATOMIC);
  var code = 'map(' + value_num + ', 0, 1024, 0, ' + value_dmax + ')';
  return [code, Blockly.Arduino.ORDER_NONE];
};

Blockly.Arduino.inout_buildin_led = function() {
  var dropdown_stat = this.getFieldValue('STAT');
  Blockly.Arduino.setups_['setup_output_13'] = 'pinMode(13, OUTPUT);';
  var code = 'digitalWrite(13, ' + dropdown_stat + ');\n'
  return code;
};

Blockly.Arduino.inout_digital_write = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  var dropdown_stat = this.getFieldValue('STAT');
  Blockly.Arduino.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
  var code = 'digitalWrite(' + dropdown_pin + ', ' + dropdown_stat + ');\n'
  return code;
};

Blockly.Arduino.inout_digital_read = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  Blockly.Arduino.setups_['setup_input_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', INPUT);';
  var code = 'digitalRead(' + dropdown_pin + ')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.inout_analog_read = function() {
  var dropdown_pin = this.getFieldValue('PIN');
  //Blockly.Arduino.setups_['setup_input_'+dropdown_pin] = 'pinMode('+dropdown_pin+', INPUT);';
  var code = 'analogRead(' + dropdown_pin + ')';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.inout_highlow = function() {
  // Boolean values HIGH and LOW.
  var code = (this.getFieldValue('BOOL') == 'HIGH') ? 'HIGH' : 'LOW';
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

/*
//servo
#include <Servo.h>

Servo servo_11;

void setup() {
  servo_11.attach(11);
}

void loop() {
servo_11.write(0);
delay(2000);

servo_11.write(150); //0~180
delay(2000);
}
*/



Blockly.Arduino.serial_print = function() {
  var content = Blockly.Arduino.valueToCode(this, 'CONTENT', Blockly.Arduino.ORDER_ATOMIC) || '0'
  //content = content.replace('(','').replace(')','');

  Blockly.Arduino.setups_['setup_serial_' + profile.default.serial] = 'Serial.begin(' + profile.default.serial + ');\n';

  var code = 'Serial.print(' + content + ');\nSerial.print("\\t");\n';
  return code;
};
