/**
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * http://blockly.googlecode.com/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating Arduino for text blocks.
 * @author gasolin@gmail.com (Fred Lin)
 */
'use strict';

goog.provide('Blockly.Arduino.texts');

goog.require('Blockly.Arduino');


Blockly.Arduino.text = function() {
  // Text value.
  var code = Blockly.Arduino.quote_(this.getFieldValue('TEXT'));
  return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.text_append = function() {
  // Append to a variable in place.
  var varName = Blockly.Arduino.variableDB_.getName(this.getTitleValue('VAR'),
      Blockly.Variables.NAME_TYPE);
  var argument0 = Blockly.Arduino.valueToCode(this, 'TEXT',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
  return varName + ' = new StringBuffer(' + varName +
      ').add(' + argument0 + ').toString();\n';
};

Blockly.Arduino.text_join = function() {
    // Create a string made up of any number of elements of any type.
    var code;
    if (this.itemCount_ == 0)
    {
        return ['\'\'', Blockly.Arduino.ORDER_ATOMIC];
    }
    else if (this.itemCount_ == 1)
    {
        code = 'String(';
        var argument0 = Blockly.Arduino.valueToCode(this, 'ADD0',
                                                    Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'';
        code += argument0;
        code += ')';
        return [code, Blockly.Arduino.ORDER_UNARY_POSTFIX];
    }
    else
    {
        code = [];
        code[0] = 'String(';
        code[0] += (Blockly.Arduino.valueToCode(this, 'ADD0',
                                                Blockly.Arduino.ORDER_NONE) || '\'\'');
        code[0] += ')';
        for (var n = 1; n < this.itemCount_; n++) {
            code[n] = '+String('
            code[n] += (Blockly.Arduino.valueToCode(this, 'ADD' + n,
                                                    Blockly.Arduino.ORDER_NONE) || '\'\'');
            code[n] += ')';
        }
        code = code.join('');
        return [code, Blockly.Arduino.ORDER_UNARY_POSTFIX];
    }
};

Blockly.Arduino.number_to_text = function () {

    var code;

    code = 'String(';
    code += (Blockly.Arduino.valueToCode(this, 'NUM',
      Blockly.Arduino.ORDER_UNARY_POSTFIX) || '\'\'');
    code += ', DEC)';
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};

Blockly.Arduino.groove_lcd_symbols = function () {

    var code;

    code = 'String((char)';
    code += this.getFieldValue('CHAR');
    code += ')';
    return [code, Blockly.Arduino.ORDER_ATOMIC];
};
