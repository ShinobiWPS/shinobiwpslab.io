/*
	Intoino's function file
*/

var opts = {
lines: 17, // The number of lines to draw
length: 0, // The length of each line
width: 5, // The line thickness
radius: 49, // The radius of the inner circle
corners: 1, // Corner roundness (0..1)
rotate: 24, // The rotation offset
direction: 1, // 1: clockwise, -1: counterclockwise
color: '#006779', // #rgb or #rrggbb or array of colors
speed: 1, // Rounds per second
trail: 64, // Afterglow percentage
shadow: false, // Whether to render a shadow
hwaccel: false, // Whether to use hardware acceleration
className: 'spinner', // The CSS class to assign to the spinner
zIndex: 2e9, // The z-index (defaults to 2000000000)
top: '50%', // Top position relative to parent
left: '50%' // Left position relative to parent
};

/**
 * Push the code for compiling operation.
 */
function pushIt(){
    tabClick("arduino");
    var arduinoName = document.getElementById('arduino_name');
    tabClick("blocks");

    if(arduinoName.value=="" || arduinoName.value.length < 4){
        alert("Inserisci un nome per il tuo iCode, deve essere lungo almeno 4 caratteri!");
    }else{
        
        tabClick("xml");
        var arduinoXML = document.getElementById('content_xml');
        
        tabClick("arduino");
        var arduinoTextarea = document.getElementById('content_arduino');
        
        tabClick("blocks");
        var target = document.getElementById('content_blocks');

        var data = {name: arduinoName.value, value: arduinoTextarea.value, xml: arduinoXML.value, email: email};
        //alert(data.name);
        //alert(data.value);
        //alert(data.xml);
        //alert(data.email);

        // Spinner
        var spinner = new Spinner(opts).spin(target);
        $(target).data('spinner', spinner);
        
        
        // send values to API
        var url = "https://be.intoino.com/editorApi/pushCode";
        //var url = "http://127.0.0.1:8080/editorApi/pushCode";
        var pushButton = document.getElementById('push_button');
        pushButton.disabled = true;
        $.post(url, data, function(response){
                console.log(response);
                if(response['log'] == "OK"){
                    alert("Compilation Done!");
                }else{
                    $(target).data('spinner').stop();
                    alert(response['log'])
                }
                  $(target).data('spinner').stop();
                pushButton.disabled = false;
                arduinoName.value = "";
            }
        );
    }

}



function stopSpinner(){
    console.log("STOP SPINNER");
    $('#content_arduino').data('spinner').stop();
}

